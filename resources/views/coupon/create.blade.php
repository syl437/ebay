@extends('layouts.app')

@section('content')
    <div class="formWrapper">
        <div class="row DirectionRtl">
            <div class="col-lg-12">
                <h1 class="page-header">הוספת קופון חדש</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row DirectionRtl" >
            <div class="col-lg-12">
                <div class="panel panel-default" >
                    <div class="panel-heading " >
                        עדכן קופון חדש
                    </div>
                    <div class="panel-body ">
                        <div class="row ">
                            <div class="col-lg-12">
                                <form role="form" action="{{route('companies.coupons.store' , $Company)}}" method="post" >
                                        <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>תיאור הקופון11</label>
                                                <input class="form-control" value="" name="title">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>מחיר הקופון</label>
                                                <input class="form-control" value="" name="price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>תיאור על הקופון</label>
                                                <input class="form-control" value="" name="description">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>כמות קופונים קיימת</label>
                                                <input class="form-control" value="" name="quantity">
                                            </div>
                                        </div>

                                    </div>


                                    <button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important;" >שלח קופון</button>
                                    {{csrf_field()}}
                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

@endsection





