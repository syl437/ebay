<?php

use Illuminate\Database\Seeder;

class PointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $points1 = new \App\Models\Point();
        $points1->quantity = 100;
        $points1->save();

        $points2 = new \App\Models\Point();
        $points2->quantity = 200;
        $points2->save();

        $points3 = new \App\Models\Point();
        $points3->quantity = 300;
        $points3->save();

        $points4 = new \App\Models\Point();
        $points4->quantity = 400;
        $points4->save();

        $points5 = new \App\Models\Point();
        $points5->quantity = 500;
        $points5->save();
    }
}
