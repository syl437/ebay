<?php

use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $media1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "hotels.jpg");
        $media2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "restaurants.jpg");
        $media3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "attractions.jpg");
        $media4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "parties.jpg");

        $cat1 = \App\CompanyCategory::find(1);
        $cat2 = \App\CompanyCategory::find(2);
        $cat3 = \App\CompanyCategory::find(3);
        $cat4 = \App\CompanyCategory::find(4);

        $cat1->attachMedia($media1, 'image');
        $cat2->attachMedia($media2, 'image');
        $cat3->attachMedia($media3, 'image');
        $cat4->attachMedia($media4, 'image');

        $media1->approved = 1;
        $media1->save();
        $media2->approved = 1;
        $media2->save();
        $media3->approved = 1;
        $media3->save();
        $media4->approved = 1;
        $media4->save();

        $usermedia1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/gofman.jpg");
        $usermedia2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/bradbury.jpg");
        $usermedia3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/gaiman.jpg");
        $usermedia4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/king.jpg");
        $usermedia5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/martin.jpg");
        $usermedia6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/munro.jpg");
        $usermedia7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/murakami.jpg");
        $usermedia8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/palahniuk.jpg");
        $usermedia9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/rowling.jpg");
        $usermedia10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_avatars/tartt.jpg");

        $user1 = \App\User::find(1); // Me
        $user2 = \App\User::find(2); // Bradbary
        $user3 = \App\User::find(3); // Gaiman
        $user4 = \App\User::find(4); // King
        $user5 = \App\User::find(5); // Martin
        $user6 = \App\User::find(6); // Munro
        $user7 = \App\User::find(7); // Murakami
        $user8 = \App\User::find(8); // Palahniuk
        $user9 = \App\User::find(9); // Rowling
        $user10 = \App\User::find(10); // Tartt

        $user1->attachMedia($usermedia1, 'avatar');
        $user2->attachMedia($usermedia2, 'avatar');
        $user3->attachMedia($usermedia3, 'avatar');
        $user4->attachMedia($usermedia4, 'avatar');
        $user5->attachMedia($usermedia5, 'avatar');
        $user6->attachMedia($usermedia6, 'avatar');
        $user7->attachMedia($usermedia7, 'avatar');
        $user8->attachMedia($usermedia8, 'avatar');
        $user9->attachMedia($usermedia9, 'avatar');
        $user10->attachMedia($usermedia10, 'avatar');

        $usermedia1->approved = 1;
        $usermedia1->save();
        $usermedia2->approved = 1;
        $usermedia2->save();
        $usermedia3->approved = 1;
        $usermedia3->save();
        $usermedia4->approved = 1;
        $usermedia4->save();
        $usermedia5->approved = 1;
        $usermedia5->save();
        $usermedia6->approved = 1;
        $usermedia6->save();
        $usermedia7->approved = 1;
        $usermedia7->save();
        $usermedia8->approved = 1;
        $usermedia8->save();
        $usermedia9->approved = 1;
        $usermedia9->save();
        $usermedia10->approved = 1;
        $usermedia10->save();

        $logo1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/aroma.gif");
        $logo2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/cafe.png");
        $logo3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/carlton.png");
        $logo4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/dominos.jpg");
        $logo5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/japanika.jpg");
        $logo6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/me.png");
        $logo7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/molly.png");
        $logo8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/saloon.png");
        $logo9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/sheraton.gif");
        $logo10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_logos/tapper.png");

        $company1 = \App\Company::find(1); // Aroma
        $company2 = \App\Company::find(2); // Cafe Cafe
        $company3 = \App\Company::find(3); // Carlton
        $company4 = \App\Company::find(4); // Dominos
        $company5 = \App\Company::find(5); // Japanika
        $company6 = \App\Company::find(6); // Museum
        $company7 = \App\Company::find(7); // Molly Blooms
        $company8 = \App\Company::find(8); // Burger Saloon
        $company9 = \App\Company::find(9); // Sheraton
        $company10 = \App\Company::find(10); // Tapper

        $company1->attachMedia($logo1, 'logo');
        $company2->attachMedia($logo2, 'logo');
        $company3->attachMedia($logo3, 'logo');
        $company4->attachMedia($logo4, 'logo');
        $company5->attachMedia($logo5, 'logo');
        $company6->attachMedia($logo6, 'logo');
        $company7->attachMedia($logo7, 'logo');
        $company8->attachMedia($logo8, 'logo');
        $company9->attachMedia($logo9, 'logo');
        $company10->attachMedia($logo10, 'logo');

        $logo1->approved = 1;
        $logo1->save();
        $logo2->approved = 1;
        $logo2->save();
        $logo3->approved = 1;
        $logo3->save();
        $logo4->approved = 1;
        $logo4->save();
        $logo5->approved = 1;
        $logo5->save();
        $logo6->approved = 1;
        $logo6->save();
        $logo7->approved = 1;
        $logo7->save();
        $logo8->approved = 1;
        $logo8->save();
        $logo9->approved = 1;
        $logo9->save();
        $logo10->approved = 1;
        $logo10->save();

       /* dispatch(new \App\Jobs\CreateLogoPin($logo1));
        dispatch(new \App\Jobs\CreateLogoPin($logo2));
        dispatch(new \App\Jobs\CreateLogoPin($logo3));
        dispatch(new \App\Jobs\CreateLogoPin($logo4));
        dispatch(new \App\Jobs\CreateLogoPin($logo5));
        dispatch(new \App\Jobs\CreateLogoPin($logo6));
        dispatch(new \App\Jobs\CreateLogoPin($logo7));
        dispatch(new \App\Jobs\CreateLogoPin($logo8));
        dispatch(new \App\Jobs\CreateLogoPin($logo9));
        dispatch(new \App\Jobs\CreateLogoPin($logo10));*/

        $video1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/aroma.mp4");
        $video2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/cafe.mp4");
        $video3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/carlton.mp4");
        $video4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/dominos.mp4");
        $video5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/japanika.mp4");
        $video6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/me.mp4");
        $video7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/molly.mp4");
        $video8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/saloon.mp4");
        $video9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_main_videos/sheraton.mp4");

        $video1->approved = 1;
        $video1->save();
        $video2->approved = 1;
        $video2->save();
        $video3->approved = 1;
        $video3->save();
        $video4->approved = 1;
        $video4->save();
        $video5->approved = 1;
        $video5->save();
        $video6->approved = 1;
        $video6->save();
        $video7->approved = 1;
        $video7->save();
        $video8->approved = 1;
        $video8->save();
        $video9->approved = 1;
        $video9->save();

       /* dispatch(new \App\Jobs\ExtractVideoThumbnail($video1));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video2));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video3));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video4));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video5));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video6));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video7));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video8));
        dispatch(new \App\Jobs\ExtractVideoThumbnail($video9));*/

        $company1->attachMedia($video1, 'main_video');
        $company2->attachMedia($video2, 'main_video');
        $company3->attachMedia($video3, 'main_video');
        $company4->attachMedia($video4, 'main_video');
        $company5->attachMedia($video5, 'main_video');
        $company6->attachMedia($video6, 'main_video');
        $company7->attachMedia($video7, 'main_video');
        $company8->attachMedia($video8, 'main_video');
        $company9->attachMedia($video9, 'main_video');

        $couponimage1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/1.jpg");
        $couponimage2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/2.jpg");
        $couponimage3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/3.jpg");
        $couponimage4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/4.jpg");
        $couponimage5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/5.jpg");
        $couponimage6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/6.jpg");
        $couponimage7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/7.jpg");
        $couponimage8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/8.jpg");
        $couponimage9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/9.jpg");
        $couponimage10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_coupons/10.jpg");

        $coupon1 = \App\Coupon::find(1);
        $coupon2 = \App\Coupon::find(3);
        $coupon3 = \App\Coupon::find(5);
        $coupon4 = \App\Coupon::find(7);
        $coupon5 = \App\Coupon::find(9);
        $coupon6 = \App\Coupon::find(11);
        $coupon7 = \App\Coupon::find(13);
        $coupon8 = \App\Coupon::find(15);
        $coupon9 = \App\Coupon::find(17);
        $coupon10 = \App\Coupon::find(19);

        $coupon1->attachMedia($couponimage1, 'coupon');
        $coupon2->attachMedia($couponimage2, 'coupon');
        $coupon3->attachMedia($couponimage3, 'coupon');
        $coupon4->attachMedia($couponimage4, 'coupon');
        $coupon5->attachMedia($couponimage5, 'coupon');
        $coupon6->attachMedia($couponimage6, 'coupon');
        $coupon7->attachMedia($couponimage7, 'coupon');
        $coupon8->attachMedia($couponimage8, 'coupon');
        $coupon9->attachMedia($couponimage9, 'coupon');
        $coupon10->attachMedia($couponimage10, 'coupon');

        $couponimage1->approved = 1;
        $couponimage1->save();
        $couponimage2->approved = 1;
        $couponimage2->save();
        $couponimage3->approved = 1;
        $couponimage3->save();
        $couponimage4->approved = 1;
        $couponimage4->save();
        $couponimage5->approved = 1;
        $couponimage5->save();
        $couponimage6->approved = 1;
        $couponimage6->save();
        $couponimage7->approved = 1;
        $couponimage7->save();
        $couponimage8->approved = 1;
        $couponimage8->save();
        $couponimage9->approved = 1;
        $couponimage9->save();
        $couponimage10->approved = 1;
        $couponimage10->save();

        $review1 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/bar1.mp4");
        $review2 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/bar2.mp4");
        $review3 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/burger1.mp4");
        $review4 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/burger2.mp4");
        $review5 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/cafe1.mp4");
        $review6 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/cafe2.mp4");
        $review7 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/cafe3.mp4");
        $review8 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/cafe4.mp4");
        $review9 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/hotel1.mp4");
        $review10 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/hotel2.mp4");
        $review11 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/hotel3.mp4");
        $review12 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/museum1.mp4");
        $review13 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/museum2.mp4");
        $review14 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/pizza1.mp4");
        $review15 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/pizza2.mp4");
        $review16 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/sushi1.mp4");
        $review17 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/sushi2.mp4");
        $review18 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/sushi3.mp4");
        $review19 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/tapper1.mp4");
        $review20 = \Plank\Mediable\MediaUploaderFacade::importPath("media", "test_reviews/tapper2.mp4");

//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review1));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review2));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review3));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review4));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review5));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review6));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review7));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review8));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review9));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review10));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review11));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review12));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review13));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review14));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review15));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review16));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review17));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review18));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review19));
//        dispatch(new \App\Jobs\ExtractVideoThumbnail($review20));

        $user1->attachMedia($review1, 'אהבתי את הפאב');
        $company7->attachMedia($review1, 'אהבתי את הפאב');

        $user4->attachMedia($review2, 'אווירה מדהימה!');
        $company7->attachMedia($review2, 'אווירה מדהימה!');

        $user5->attachMedia($review3, 'בורגרים מצוינים');
        $company8->attachMedia($review3, 'בורגרים מצוינים');

        $user3->attachMedia($review4, 'המקומ מה זה טוב');
        $company8->attachMedia($review4, 'המקומ מה זה טוב');

        $user6->attachMedia($review5, 'קפה פה נהדר');
        $company1->attachMedia($review5, 'קפה פה נהדר');

        $user9->attachMedia($review6, 'סנדויצ`ים מעוד טעימים');
        $company1->attachMedia($review6, 'סנדויצ`ים מעוד טעימים');

        $user10->attachMedia($review7, 'אהבתי את המקום');
        $company2->attachMedia($review7, 'אהבתי את המקום');

        $user2->attachMedia($review8, 'סלטים טובים וטריים');
        $company2->attachMedia($review8, 'סלטים טובים וטריים');

        $user1->attachMedia($review9, 'בית מלון מעולה לזוגים צעירים');
        $company3->attachMedia($review9, 'בית מלון מעולה לזוגים צעירים');

        $user7->attachMedia($review10, 'מקום יפה וטוב');
        $company3->attachMedia($review10, 'מקום יפה וטוב');

        $user8->attachMedia($review11, 'ספא מצוין');
        $company9->attachMedia($review11, 'ספא מצוין');

        $user10->attachMedia($review12, 'אהבתי את התצוגה');
        $company6->attachMedia($review12, 'אהבתי את התצוגה');

        $user6->attachMedia($review13, 'התרבות שלנו');
        $company6->attachMedia($review13, 'התרבות שלנו');

        $user4->attachMedia($review14, 'פיצה הכי טובה בארץ');
        $company4->attachMedia($review14, 'פיצה הכי טובה בארץ');

        $user8->attachMedia($review15, 'פיצה מרגריטה זה משהו');
        $company4->attachMedia($review15, 'פיצה מרגריטה זה משהו');

        $user3->attachMedia($review16, 'דג טרי, אורז טעים');
        $company5->attachMedia($review16, 'דג טרי, אורז טעים');

        $user5->attachMedia($review17, 'אהבתי את הסושי פה');
        $company5->attachMedia($review17, 'אהבתי את הסושי פה');

        $user4->attachMedia($review18, 'מקום מה זה טעים');
        $company5->attachMedia($review18, 'מקום מה זה טעים');

        $user1->attachMedia($review19, 'עובדים טוב-טוב');
        $company10->attachMedia($review19, 'עובדים טוב-טוב');

        $user8->attachMedia($review20, 'אפליקציות יפות ומהירות');
        $company10->attachMedia($review20, 'אפליקציות יפות ומהירות');

        $review1->approved = 1;
        $review1->save();
        $review2->approved = 1;
        $review2->save();
        $review3->approved = 1;
        $review3->save();
        $review4->approved = 1;
        $review4->save();
        $review5->approved = 1;
        $review5->save();
        $review6->approved = 1;
        $review6->save();
        $review7->approved = 1;
        $review7->save();
        $review8->approved = 1;
        $review8->save();
        $review9->approved = 1;
        $review9->save();
        $review10->approved = 1;
        $review10->save();
        $review11->approved = 1;
        $review11->save();
        $review12->approved = 1;
        $review12->save();
        $review13->approved = 1;
        $review13->save();
        $review14->approved = 1;
        $review14->save();
        $review15->approved = 1;
        $review15->save();
        $review16->approved = 1;
        $review16->save();
        $review17->approved = 1;
        $review17->save();
        $review18->approved = 1;
        $review18->save();
        $review19->approved = 1;
        $review19->save();
        $review20->approved = 1;
        $review20->save();
    }
}
