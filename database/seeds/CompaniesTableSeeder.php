<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company1 = new \App\Company();
        $company1->company_subcategory()->associate(\App\CompanySubcategory::find(8));
        $company1->region()->associate(\App\Region::find(3));
        $company1->title = 'ארומה';
        $company1->address = 'Sderot Yerushalayim 4, Beersheba, Israel';
        $company1->country = 'IL';
        $company1->lat = 31.245769;
        $company1->lng = 34.781407;
        $company1->website = '';
        $company1->phone = '+97286104002';
        $company1->description = 'ארומה אספרסו-בר ישראל';
        $company1->approved = 1;
        $company1->save();

        $company2 = new \App\Company();
        $company2->company_subcategory()->associate(\App\CompanySubcategory::find(8));
        $company2->region()->associate(\App\Region::find(4));
        $company2->title = 'קפה קפה';
        $company2->address = 'Derech Pa\'amei HaShalom, Eilat';
        $company2->country = 'IL';
        $company2->lat = 29.549865;
        $company2->lng = 34.953988;
        $company2->website = '';
        $company2->phone = '+97286339643';
        $company2->description = 'הכירו את החוויה הקולינרית החדשה של קפה קפה. חומרי הגלם האיכותיים ביותר ומגע ידו של השף, היוצרים חוויה קולינרית חדשה של ניחוחות וטעמים.';
        $company2->approved = 1;
        $company2->save();

        $company3 = new \App\Company();
        $company3->company_subcategory()->associate(\App\CompanySubcategory::find(3));
        $company3->region()->associate(\App\Region::find(2));
        $company3->title = 'Carlton';
        $company3->address = 'Eliezer Peri St 10, Tel Aviv-Yafo, Israel';
        $company3->country = 'IL';
        $company3->lat = 32.086512;
        $company3->lng = 34.769567;
        $company3->website = '';
        $company3->phone = '+97235201818';
        $company3->description = 'מלון קרלטון תל-אביב מציע חוויה קולינארית מושלמת, החל מארוחת בוקר שזכתה בפרסים, מבחר גבינות, יינות ובירות בוטיק ועד לשתי מסעדות בניהולו של השף בעל המוניטין הבינלאומי';
        $company3->approved = 1;
        $company3->save();

        $company4 = new \App\Company();
        $company4->company_subcategory()->associate(\App\CompanySubcategory::find(5));
        $company4->region()->associate(\App\Region::find(1));
        $company4->title = 'דומינוס פיצה';
        $company4->address = 'Shalom Aleichem St 1, Haifa, Israel';
        $company4->country = 'IL';
        $company4->lat = 32.783081;
        $company4->lng = 35.014907;
        $company4->website = '';
        $company4->phone = '+972768048974';
        $company4->description = 'הפיצה הטובה בישראל! לא משנה היכן אתם נמצאים – הזמינו עכשיו ואנחנו נגיע בתוך 30 דקות! למבצעים, משלוחים והזמנות, הכנסו לאתר דומינוס פיצה';
        $company4->approved = 1;
        $company4->save();

        $company5 = new \App\Company();
        $company5->company_subcategory()->associate(\App\CompanySubcategory::find(7));
        $company5->region()->associate(\App\Region::find(1));
        $company5->title = 'ג\'פניקה';
        $company5->address = 'HaHistadrut Ave 248, Haifa, Israel';
        $company5->country = 'IL';
        $company5->lat = 32.810341;
        $company5->lng = 35.073584;
        $company5->website = '';
        $company5->phone = '+97237236100';
        $company5->description = 'גפניקה מתמחה בנפלאות המטבח האסייתי. העקרונות המנחים אותנו הם שילוב של מוצרים טריים ואיכותיים לצד מחירים נוחים ותודעת שירות גבוהה.';
        $company5->approved = 1;
        $company5->save();

        $company6 = new \App\Company();
        $company6->company_subcategory()->associate(\App\CompanySubcategory::find(9));
        $company6->region()->associate(\App\Region::find(2));
        $company6->title = 'מוזיאון ארץ ישראל';
        $company6->address = 'Sderot Yerushalayim 4, Beersheba, Israel';
        $company6->country = 'IL';
        $company6->lat = 32.103143;
        $company6->lng = 34.796216;
        $company6->website = '';
        $company6->phone = '+97236415244';
        $company6->description = 'מוזיאון ארץ ישראל הוא מוזיאון רב-תחומי העוסק בתולדות הארץ ותרבותה באמצעות תצוגת קבע נרחבת ותערוכות מתחלפות';
        $company6->approved = 1;
        $company6->save();

        $company7 = new \App\Company();
        $company7->company_subcategory()->associate(\App\CompanySubcategory::find(4));
        $company7->region()->associate(\App\Region::find(2));
        $company7->title = 'מולי בלומס פאב אירי';
        $company7->address = 'Mendele Mokher Sfarim St 2, Tel Aviv-Yafo, Israel';
        $company7->country = 'IL';
        $company7->lat = 32.072828;
        $company7->lng = 34.787003;
        $company7->website = '';
        $company7->phone = '+97235221558';
        $company7->description = 'ל ערב נפתח עם מוזיקה אירית מסורתית. בימי שני ורביעי מתקיימות הופעות חיות של חלק מההרכב האירי קטיפה שחורה.';
        $company7->approved = 1;
        $company7->save();

        $company8 = new \App\Company();
        $company8->company_subcategory()->associate(\App\CompanySubcategory::find(6));
        $company8->region()->associate(\App\Region::find(2));
        $company8->title = 'בורגר סאלון';
        $company8->address = 'Derech HaMaccabim 67, Rishon LeTsiyon, Israel';
        $company8->country = 'IL';
        $company8->lat = 31.991374;
        $company8->lng = 34.812090;
        $company8->website = '';
        $company8->phone = '1700705715';
        $company8->description = 'בורגר סאלון הינה רשת מסעדות המתמחה בהגשת המבורגרים ומנות מוכרות ואהובות נוספות, באיכות המעולה ביותר.';
        $company8->approved = 1;
        $company8->save();

        $company9 = new \App\Company();
        $company9->company_subcategory()->associate(\App\CompanySubcategory::find(2));
        $company9->region()->associate(\App\Region::find(2));
        $company9->title = 'Sheraton Tel Aviv';
        $company9->address = 'HaYarkon St 115, Tel Aviv-Yafo, Israel';
        $company9->country = 'IL';
        $company9->lat = 32.081351;
        $company9->lng = 34.768162;
        $company9->website = '';
        $company9->phone = '+97235211111';
        $company9->description = 'מלון שרתון תל אביב ממוקם אל מול חופי הים התיכון, בלבה של תל אביב, מרחק פסיעה ממרכזי הבילוי, מסעדות, ברים, שווקים ומרכזי הקניות.';
        $company9->approved = 1;
        $company9->save();

        $company10 = new \App\Company();
        $company10->company_subcategory()->associate(\App\CompanySubcategory::find(14));
        $company10->region()->associate(\App\Region::find(1));
        $company10->title = 'Tapper';
        $company10->address = 'Sderot HaHistadrut 251, Haifa, Israel';
        $company10->country = 'IL';
        $company10->lat = 32.810055;
        $company10->lng = 35.068205;
        $company10->website = '';
        $company10->phone = '+972509666662';
        $company10->description = 'חברה לפיתוח אתרים ואפליקציות לאנדרואיד , אנו מתמחים בפיתוח משחקים ואפליקציות לאנדרואיד ,בניית אפליקציות מבוססות מיקום והודעות push';
        $company10->approved = 1;
        $company10->save();
    }
}
