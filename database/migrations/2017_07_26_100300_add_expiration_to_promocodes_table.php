<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExpirationToPromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('promocodes.table', 'promocodes'), function (Blueprint $table) {
            $table->timestamp('expiration')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('promocodes.table', 'promocodes'), function (Blueprint $table) {
            $table->dropColumn('expiration');
        });
    }
}
